#!/bin/bash
eval $(cat vm.config)
echo "👋 Registering 🦊🛠 ${runner_name}..."

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
  gitlab-runner register --non-interactive \\
    --url ${gitlab_url} \\
    --name ${runner_name} \\
    --registration-token ${gitlab_token} \\
    --executor docker \\
    --docker-image alpine:latest
EOF
