# Docker runner

## About

How to install a GitLab (Docker) Runner with Multipass.

## Setup

- Got to the CI/CD Settings of your group or your project
- Copy the GitLab URL and the GitLab token to the `vm.config` file (rename the `vm.sample.config` file)
- Run `./create.runner.sh`
- Then, run `./register.runner.sh`

## Other commands

- stop the runner's vm: `./stop.sh`
- start the runner's vm: `./start.sh`
- destroy the runner's vm: `./remove.sh`


