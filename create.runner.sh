#!/bin/bash
eval $(cat vm.config)
echo "👋 creating 🦊🛠 ${runner_name}..."

multipass launch --name ${vm_name} \
  --cpus ${vm_cpus} \
	--mem ${vm_mem} \
	--disk ${vm_disk}

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
apt-get update
apt install docker.io -y
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
apt-get install -y gitlab-runner
EOF

echo "🖐️ 😃 📦 Runner initialized on ${vm_name} ✅"
